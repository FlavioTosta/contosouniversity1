using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;

namespace ContosoUniversity.DAL 
{
    public interface IUnitOfWork 
    {
        Task<int> Save();
        IDbContextTransaction BeginTransaction();        
    }
}