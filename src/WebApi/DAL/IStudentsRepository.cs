using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ContosoUniversity.Models;

namespace ContosoUniversity.DAL 
{
    public interface IStudentsRepository
    {       
        Task<InquiryResponse<Student>> FetchAllAsync(InquiryRequest request);
        Task<Response<Student>> GetByIdAsync(int id);
        void Insert(Student entity);
        void Delete(int id);
        void Delete(Student entityToDelete);
        void Update(Student entityToUpdate);
        IQueryable<Student> GetQueryable();
    }
}