using System;
using System.Collections.Generic;
using System.Linq;
using ContosoUniversity.Data;
using ContosoUniversity.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace ContosoUniversity.DAL
{
    /// <summary>
    /// Action filter responsible for handling 
    /// </summary>
    public class UnitOfWorkFilterAttribute : ActionFilterAttribute
    {
        private readonly SchoolContext context;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly IStringLocalizer<SharedResources> sharedLocalizer;
        private ILoggerFactory loggerFactory;
        private ILogger logger;

        /// <summary>
        /// Retrieve response object from the action context
        /// </summary>
        /// <param name="filterContext"></param>
        /// <returns></returns>
        private Response retrieveResponse(ActionExecutedContext filterContext)
        {
            Response response = null;

            if (filterContext.Exception != null)
            {
                response = new Response { OperationSuccess = false };
                response.AddDefaultErrorMessage();
                if (hostingEnvironment.IsDevelopment())
                {
                    response.AddMessage(filterContext.Exception);
                }

                logger = loggerFactory.CreateLogger(filterContext.ActionDescriptor.DisplayName);
                logger.LogError(filterContext.Exception, "");

                return response;
            }

            try
            {
                var result = filterContext.Result as ObjectResult;
                response = (Response)result.Value;
                return response;
            }
            catch (NullReferenceException e)
            {
                logger = loggerFactory.CreateLogger(filterContext.ActionDescriptor.DisplayName);
                logger.LogWarning(e, "Action does not return an object.");
                return null;
            }
            catch (InvalidCastException e)
            {
                logger = loggerFactory.CreateLogger(filterContext.ActionDescriptor.DisplayName);
                logger.LogWarning(e, "Action does not return an object of type Response.");
                return null;
            }
        }

        private void shortCircuitFilterExecution(ActionExecutedContext ctx, Response response)
        {
            if (response == null) return;            
            if (!response.OperationSuccess && !response.Messages.Any(m => m.Code == "E0001")) response.AddDefaultErrorMessage();
            this.LocalizeMessages(ctx, response);
            ctx.Result = new ObjectResult(response);                        
        }

        /// <summary>
        /// Localize error messages which have a code.
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="response"></param>
        private void LocalizeMessages(ActionExecutedContext ctx, Response response)
        {
            logger = loggerFactory.CreateLogger(ctx.ActionDescriptor.DisplayName);

            response.Messages.ForEach(m =>
            {
                if (m.Code == null) return;   

                try 
                {
                    m.Description = m.Arguments != null ?
                        this.sharedLocalizer[m.Code, m.Arguments.ToArray()] : this.sharedLocalizer[m.Code];  
                }
                catch (FormatException e) 
                {                 
                    logger.LogWarning(e, "Invalid number of arguments passed to localize: " + m.Code);
                }             
                              
            });
        }

        /// <summary>
        /// Checkes whether the suplied actionName should open a db transaction
        /// </summary>
        /// <param name="actionName"></param>
        /// <returns></returns>         
        private bool isTransactionAction(string actionName)
        {
            var transactionActionNames = new List<string> { "CREATE", "UPDATE", "DELETE" };
            actionName = actionName.Substring(actionName.LastIndexOf(".")).ToUpper();
            var result = transactionActionNames.Any(t => actionName.Contains(t));
            return result;
        }

        /// <summary>
        /// Checks whether there is a database transaction opened
        /// </summary>
        /// <returns>bool</returns>
        private bool isTransactionOpen
        {
            get
            {
                return this.context.Database.CurrentTransaction != null;
            }
        }

        /// <summary>
        /// Rollback current opened transaction
        /// </summary>
        private void rollBackTransaction()
        {
            Console.WriteLine("Rollback transaction");
            this.context.Database.RollbackTransaction();
        }

        /// <summary>
        /// Checks whether the opened transaction for the current request should be rolled back
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        private bool shouldRollbackTransaction(ActionExecutedContext ctx, Response response)
        {
            return ctx.Exception != null || !response.OperationSuccess;
        }

        /// <summary>
        /// Commit current opened transaction
        /// </summary>
        private void commitTransaction()
        {
            Console.WriteLine("Commit transaction");
            this.context.SaveChanges();
            this.context.Database.CommitTransaction();
        }

        /// <summary>
        /// Returns a response object containing error messages related to a failed attempt to commit a transaction
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="transactionException"></param>        
        /// <returns>a response object</returns>
        private Response handleTransactionErrorMessages(ActionExecutedContext ctx, Exception transactionException)
        {
            Console.WriteLine("Commit transaction failed");

            var response = new Response();
            response.OperationSuccess = false;
            response.Messages.Add(new Message
            {
                Type = MessageType.ERROR,
                Code = "E0001"
            });

            if (hostingEnvironment.IsDevelopment())
            {
                response.AddMessage(transactionException);
            }

            logger = loggerFactory.CreateLogger(ctx.ActionDescriptor.DisplayName);
            logger.LogError(ctx.Exception, "");

            return response;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="hostingEnvironment"></param>
        /// <param name="sharedLocalizer"></param>
        /// <param name="loggerFactory"></param>
        public UnitOfWorkFilterAttribute(
            SchoolContext context,
            IHostingEnvironment hostingEnvironment,
            IStringLocalizer<SharedResources> sharedLocalizer,
            ILoggerFactory loggerFactory)
        {
            this.context = context;
            this.hostingEnvironment = hostingEnvironment;
            this.sharedLocalizer = sharedLocalizer;
            this.loggerFactory = loggerFactory;
        }

        /// <summary>
        /// Action filter that runs in the beggining of the request
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (isTransactionAction(filterContext.ActionDescriptor.DisplayName))
            {
                Console.WriteLine("Begin transaction");
                this.context.Database.BeginTransaction();
            }
        }

        /// <summary>
        /// Action filter that runs in end of the request 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var response = this.retrieveResponse(filterContext);

            if (!this.isTransactionOpen)
            {
                shortCircuitFilterExecution(filterContext, response);
                return;
            }

            if (this.shouldRollbackTransaction(filterContext, response))
            {
                this.rollBackTransaction();
                shortCircuitFilterExecution(filterContext, response);
                return;
            }

            try
            {
                this.commitTransaction();
            }
            catch (Exception e)
            {
                var transactionErrorsResponse = handleTransactionErrorMessages(filterContext, e);
                response.Merge(transactionErrorsResponse);

                shortCircuitFilterExecution(filterContext, response);
            }

        }
    }
}