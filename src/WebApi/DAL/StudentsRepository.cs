using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ContosoUniversity.Data;
using ContosoUniversity.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace ContosoUniversity.DAL 
{
    public class StudentsRepository : IStudentsRepository
    {
        private SchoolContext context { get; set; }
        private DbSet<Student> dbSet { get; set; }

        public StudentsRepository(SchoolContext context)
        {            
            this.context = context;
            this.dbSet = context.Set<Student>();    
        }

        public async Task<InquiryResponse<Student>>  FetchAllAsync(InquiryRequest request) {

            var query = this.dbSet.AsQueryable();
            Expression<Func<Student, bool>> filter = Utilities.BuildFilterExpr<Student>(request.Filters);
            Func<IQueryable<Student>, IOrderedQueryable<Student>> orderBy = Utilities.BuildOrderByFunc<Student>(query, request.SortExpressions);

            string includePropertiesValue = string.Empty;
            
            if (request.Filters != null) 
            {
                var includeProperties = request.Filters.FirstOrDefault(f => f.Name == "IncludeProperties");            
                includePropertiesValue = includeProperties != null ? includeProperties.Value : "";
            }            

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includePropertiesValue.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            PaginatedList<Student> results = await PaginatedList<Student>.CreateAsync(query.AsNoTracking(), request.Page ?? 1, request.PageSize ?? 1);  
            return new InquiryResponse<Student>{
                Page = results.PageIndex,
                PageSize = request.PageSize ?? 1,
                Total = results.Count,
                Items = results
            };
        }

         public async virtual Task<Response<Student>> GetByIdAsync(int id)
        {
            var response = new Response<Student>();            
            Student item = await dbSet.FindAsync(id);
            response.Item = item;
            return response;
        }

        public virtual void Insert(Student entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(int id)
        {
            Student entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(Student entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(Student entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public IQueryable<Student> GetQueryable() 
        {
            return this.context.Students.AsQueryable();
        }
    }
}