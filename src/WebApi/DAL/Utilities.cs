using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ContosoUniversity.Models;
using Microsoft.EntityFrameworkCore;

namespace ContosoUniversity.DAL {
    public static class Utilities 
    {
         public static Expression<Func<TEntity, bool>> BuildFilterExpr<TEntity>(List<Filter> filters)
        {
            if (filters == null) return null; 
            ParameterExpression pe = Expression.Parameter(typeof(TEntity), "s");
            Expression e = null, right, left;
            Expression<Func<TEntity, bool>> filter = null;

            foreach (var f in filters)
            {
                left = Expression.Property(pe, typeof(TEntity).GetProperty(f.Name));
                right = Expression.Constant(f.Value);

                if (e == null)
                {
                    switch (f.ComparerType)
                    {
                        case ComparerType.EQUAL:
                            e = Expression.Equal(left, right);
                            break;
                        case ComparerType.NOT_EQUAL:
                            e = Expression.NotEqual(left, right);
                            break;
                        case ComparerType.GREATER_THAN:
                            e = Expression.NotEqual(left, right);
                            break;
                        case ComparerType.LESS_THAN:
                            e = Expression.NotEqual(left, right);
                            break;
                    }
                    continue;
                }

                switch (f.Condition)
                {
                    case Condition.OR:
                        e = Expression.OrElse(e, Expression.Equal(left, right));
                        break;
                    default:
                        e = Expression.AndAlso(e, Expression.Equal(left, right));
                        break;
                }
            }

            if (e != null)
            {
                filter = Expression.Lambda<Func<TEntity, bool>>(e, pe);
            }

            return filter;
        }

        public static Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> BuildOrderByFunc<TEntity>(IQueryable<TEntity> query, List<SortExpression> sortExpressions)
        {
            if (sortExpressions == null || query == null) return null; 
            IOrderedQueryable<TEntity> orderBy = null;
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderExpr = null;

            foreach (var sortEx in sortExpressions.OrderByDescending(s => s.Priority))
            {

                if (orderBy == null)
                {
                    if (sortEx.Order == Order.ASC)
                    {
                        orderBy = query.OrderBy(s => EF.Property<object>(s, sortEx.Name));
                        continue;
                    }

                    orderBy = query.OrderByDescending(s => EF.Property<object>(s, sortEx.Name));
                }
                else
                {
                    if (sortEx.Order == Order.ASC)
                    {
                        orderBy = orderBy.ThenBy(s => EF.Property<object>(s, sortEx.Name));
                        continue;
                    }

                    orderBy = orderBy.ThenByDescending(s => EF.Property<object>(s, sortEx.Name));
                }
            }

            if (orderBy != null)
            {
                orderExpr = q => orderBy;
            }

            return orderExpr;
        }
    }
}