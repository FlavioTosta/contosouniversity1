using System.Threading.Tasks;
using ContosoUniversity.Data;
using Microsoft.EntityFrameworkCore.Storage;

namespace ContosoUniversity.DAL {
    public class UnitOfWork : IUnitOfWork
    {        
        
        private readonly SchoolContext context;

        public UnitOfWork(SchoolContext context) {
            this.context = context;
        }

        public async Task<int> Save()
        {
            return await context.SaveChangesAsync();
        }

        public IDbContextTransaction BeginTransaction()
        {
            return this.context.Database.BeginTransaction();
        }
    }
}