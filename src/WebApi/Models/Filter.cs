namespace ContosoUniversity.Models
{
    public enum Condition {
        AND,
        OR
    }

    public enum ComparerType {
        EQUAL,
        NOT_EQUAL,
        GREATER_THAN,
        LESS_THAN,
        GREATER_THAN_OR_EQUAL_TO,
        LESS_THAN_OR_EQUAL_TO
    }

    public class Filter
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public Condition Condition { get; set; }

        public ComparerType ComparerType { get; set; }
    }
}