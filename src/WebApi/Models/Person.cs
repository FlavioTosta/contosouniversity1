using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{
    public abstract class Person : BaseModel
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(50)]        
        public string LastName { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(50, ErrorMessage = "First name cannot be longer than {0} characters")]
        [Column("FirstName")]
        public string FirstMidName { get; set; }

        [Display(Name="Full Name")]
        public string FullName
        {
            get
            {
                if (this.LastName == null || this.FirstMidName == null) 
                {
                    return null;
                }
                return LastName + ", " + FirstMidName;
            }
        }
    }
}