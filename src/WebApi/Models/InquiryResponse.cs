using System.Collections;
using System.Collections.Generic;

namespace ContosoUniversity.Models {
    public class InquiryResponse<T> : Response
    {
        public int Total { get; set; }        
        public int Page { get; set; }
        public int PageSize { get; set; }
        public IEnumerable<T> Items { get; set; }       
    }    
}