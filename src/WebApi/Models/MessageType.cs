namespace ContosoUniversity.Models
{
    public enum MessageType
    {
        ERROR,
        INFO,
        VALIDATION,        
        WARNING        
    }
}