using System.Collections.Generic;
using Newtonsoft.Json;

namespace ContosoUniversity.Models {
    public class Message 
    {
        public string Description { get; set; }

        [JsonIgnore]
        public string Code { get; set; }

        public MessageType Type { get; set; }

        [JsonIgnore]
        public List<string> Arguments { get; set; }

        public string PropertyName { get; set; }
    }
}