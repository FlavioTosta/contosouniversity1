using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ContosoUniversity.Models
{
    public class Response
    {        
        private const string defaultErrorMessage = "E0001";

        private bool operationSuccess = true;
        
        private List<Message> messages = new List<Message>();
        
        /// <summary>
        /// Whether it is a successful or error response object.
        /// Normally when false, will contain error messages
        /// </summary>
        public bool OperationSuccess
        {
            get { return operationSuccess; }
            set { operationSuccess = value; }
        }

        /// <summary>
        /// List of response messages
        /// </summary>
        /// <returns></returns>
        public List<Message> Messages
        {
            get { return messages; }
            set { messages = value; }
        }

        /// <summary>
        /// Merges the supplied response obj to this.
        /// Adds messages from supplied response obj to this.
        /// If supplied response obj OperationSuccess is false, this becomes false
        /// </summary>
        /// <param name="response"></param>
        public virtual void Merge(Response response)
        {
            if (this.operationSuccess == true && response.OperationSuccess == false)
            {
                this.operationSuccess = false;
            }
            this.messages.AddRange(response.Messages);
        }

        /// <summary>
        /// Adds a message to the current object, containing the description from the exception supplied
        /// </summary>
        /// <param name="e"></param>
        public virtual void AddMessage(Exception e) 
        {
            this.Messages.Add(new Message {
                Type = MessageType.ERROR,
                Description = e.Message
            });
        }

        public virtual void AddMessage(Message msg) 
        {
            if (this.messages.Any(m => m.Code == defaultErrorMessage)) this.AddDefaultErrorMessage();
            this.messages.Add(msg);
        }

        /// <summary>
        /// Adds default error message to list of messages in the current response object
        /// </summary>
        public virtual void AddDefaultErrorMessage() 
        {
            if (this.messages.Any(m => m.Code == defaultErrorMessage)) return;

            this.messages.Insert(0, new Message{
                Type = MessageType.ERROR,
                Code = defaultErrorMessage
            });
        }

        /// <summary>
        /// Adds validation error messages to the current response object
        /// </summary>
        /// <param name="errors"></param>
        public void Merge(List<ValidationResult> errors)
        {
            foreach(var error in errors) 
            {
                if (this.Messages.Any(m => m.Description == error.ErrorMessage)) {
                    continue; 
                } 
                this.Messages.Add(new Message{
                    Type = MessageType.VALIDATION,
                    Description = error.ErrorMessage,
                    PropertyName = error.MemberNames.FirstOrDefault()
                });
            }   
        }
    }

    public class Response<T> : Response
    {
        public T Item { get; set; }

        public void Merge(Response<T> response) {
            base.Merge(response);

            if (this.Item == null && response.Item != null) 
            {
                this.Item = response.Item;
            }
        }
    }
}