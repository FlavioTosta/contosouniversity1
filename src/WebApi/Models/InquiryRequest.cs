using System.Collections;
using System.Collections.Generic;

namespace ContosoUniversity.Models {
    public class InquiryRequest {
        public List<Filter> Filters { get; set; }
        public List<SortExpression> SortExpressions { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
    }
}