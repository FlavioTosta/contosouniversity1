using System.Collections;
using System.Collections.Generic;

namespace ContosoUniversity.Models {

    public enum Order {
        ASC,
        DESC
    }

    public class SortExpression 
    {
        public string Name { get; set; }
        public int Priority { get; set; }
        public Order Order { get; set; }        
    }
}