using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;

namespace ContosoUniversity.Models
{
    public abstract class BaseModel
    {
        // public DateTime CreatedDate { get; set; }
        // public int? CreatedBy { get; set; }
        // public DateTime ModifiedDate { get; set; }
        // public int? ModifiedBy { get; set; }
        
        private List<ValidationResult> validationErrors = new List<ValidationResult>();

        [NotMapped]
        [JsonIgnore]
        public List<ValidationResult> ValidationErrors { 
            get 
            {
                return this.validationErrors;
            }            
            set 
            {
                this.validationErrors = value;
            }
        }
        
        [NotMapped]
        private bool isValid = true;
        
        public bool IsValid(ModelStateDictionary modelState)
        {
            if (!modelState.IsValid) 
            {
                ValidationResult validation = null;
                foreach (var error in modelState)
                {
                    validation = new ValidationResult(error.Value.Errors.FirstOrDefault().ErrorMessage,
                     new List<string>() { error.Key });
                    this.ValidationErrors.Add(validation);
                }   
                return false;
            }

            return true;
        }

    }
}