using ContosoUniversity.Models;
using ContosoUniversity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
{
    private readonly IHostingEnvironment hostingEnvironment;
    private readonly IModelMetadataProvider modelMetadataProvider;
    private ILogger logger;
    private ILoggerFactory loggerFactory;
    private readonly IStringLocalizer<SharedResources> sharedLocalizer;

    public CustomExceptionFilterAttribute(
        IHostingEnvironment hostingEnvironment,
        IModelMetadataProvider modelMetadataProvider,
        ILoggerFactory loggerFactory,
        IStringLocalizer<SharedResources> sharedLocalizer)
    {
        this.hostingEnvironment = hostingEnvironment;
        this.modelMetadataProvider = modelMetadataProvider;
        this.loggerFactory = loggerFactory;
        this.sharedLocalizer = sharedLocalizer;
    }

    public IHostingEnvironment HostingEnvironment => hostingEnvironment;

    public override void OnException(ExceptionContext context)
    {
        var response = new Response{ OperationSuccess = false };
        response.AddDefaultErrorMessage();

        if (hostingEnvironment.IsDevelopment())
        {               
            response.AddMessage(context.Exception);
        }
        
        logger = loggerFactory.CreateLogger(context.ActionDescriptor.DisplayName);
        logger.LogError(context.Exception, "");

        // Localize messages which have a code
        response.Messages.ForEach(m => m.Description = m.Code != null ? m.Description = sharedLocalizer[m.Code] : m.Description);
        context.Result = new ObjectResult(response);
            return;
    }
}