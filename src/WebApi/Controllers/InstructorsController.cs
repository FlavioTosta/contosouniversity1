using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ContosoUniversity.Data;
using ContosoUniversity.Models;
using ContosoUniversity.Models.SchoolViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ContosoUniversity.Controllers
{
    [Route("api/instructors")]
    public class InstructorsController : Controller
    {
        private readonly SchoolContext _context;
        public InstructorsController(SchoolContext context)
        {
            _context = context;
        }

        [HttpPost("fetchAll")]
        public async Task<IActionResult> Index([FromBody] InquiryRequest request)
        {
                var viewModel = new InstructorIndexData();
                var instructors = _context.Instructors
                      .Include(i => i.OfficeAssignment)
                      .Include(i => i.CourseAssignments)
                        .ThenInclude(i => i.Course)
                            .ThenInclude(i => i.Department)
                      .AsNoTracking();

                var sortExpressions = request.SortExpressions.OrderByDescending(s => s.Priority);
                foreach (var sortEx in sortExpressions)
                {
                    if (sortEx.Order == Order.ASC)
                    {
                        instructors = instructors.OrderBy(e => EF.Property<object>(e, sortEx.Name));
                        continue;
                    }

                    instructors = instructors.OrderByDescending(e => EF.Property<object>(e, sortEx.Name));
                }

                viewModel.Instructors = await instructors.ToListAsync();

                var id = request.Filters.FirstOrDefault(f => f.Name == "Id");
                var courseId = request.Filters.FirstOrDefault(f => f.Name == "CourseId");

                if (id != null)
                {
                    Instructor instructor = viewModel.Instructors.Where(i => i.Id == int.Parse(id.Value)).Single();
                    viewModel.Courses = instructor.CourseAssignments.Select(s => s.Course);
                }

                if (courseId != null)
                {
                    var selectedCourse = viewModel.Courses.Where(x => x.CourseId == int.Parse(courseId.Value)).Single();
                    await _context.Entry(selectedCourse).Collection(x => x.Enrollments).LoadAsync();
                    foreach (Enrollment enrollment in selectedCourse.Enrollments)
                    {
                        await _context.Entry(enrollment).Reference(x => x.Student).LoadAsync();
                    }
                    viewModel.Enrollments = selectedCourse.Enrollments;
                }

                var response = new Response<InstructorIndexData>();
                response.OperationSuccess = true;
                response.Item = viewModel;
                return new ObjectResult(response);
        }
    }
}