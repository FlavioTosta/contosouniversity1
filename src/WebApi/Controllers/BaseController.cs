using System;
using ContosoUniversity.Models;
using Microsoft.AspNetCore.Mvc;

namespace ContosoUniversity.Controllers {
    public abstract class BaseController : Controller
    {
        /// <summary>
        /// Localizes the exception error message and merges to the existing response object
        /// </summary>
        /// <param name="response"></param>
        /// <param name="messageCode"></param>
        /// <param name="e"></param>
        protected void HandleExceptions (Response response, string messageCode, Exception e) 
        {

        }
    }    
}