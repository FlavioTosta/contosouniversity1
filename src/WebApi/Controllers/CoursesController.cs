using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ContosoUniversity.Data;
using ContosoUniversity.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ContosoUniversity.Controllers
{
    [Route("api/courses")]
    public class CoursesController : Controller
    {
        private readonly SchoolContext _context;
        public CoursesController(SchoolContext context)
        {
            _context = context;
        }

        [HttpPost("fetchAll")]
        public async Task<IActionResult> Index()
        {
            var courses = _context.Courses.Include(c => c.Department)
                .AsNoTracking();

            var result = await courses.ToListAsync();
            return new ObjectResult(result);
        }
    }
}