using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ContosoUniversity.Models;
using ContosoUniversity.DAL;
using ContosoUniversity.BAL;
using System;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace ContosoUniversity.Controllers
{
    [Route("api/students")]
    public class StudentsController : BaseController
    {
        private readonly IStudentsRepository studentRepo;
        private readonly IStudentsBAL studentsBAL;
        private readonly ILogger<StudentsController> logger;

        public StudentsController(
            IStudentsRepository studentRepo,
            IStudentsBAL studentsBAL,
            ILogger<StudentsController> logger)
        {
            this.studentRepo = studentRepo;
            this.studentsBAL = studentsBAL;
            this.logger = logger;
        }

        [HttpPost("fetchAll")]
        public async Task<IActionResult> FetchAll([FromBody] InquiryRequest request)
        {
            var response = await this.studentsBAL.FetchAllAsync(request);
            return new ObjectResult(response);
        }

        [Authorize]
        [HttpGet("{id}", Name = "GetById")]
        public async Task<IActionResult> GetById(int id)
        {
            var response = await this.studentRepo.GetByIdAsync(id);
            return new ObjectResult(response);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Student student)
        {
            var response = new Response<Student>();

            if (student == null)
            {
                return BadRequest();
            }

            if (!student.IsValid(this.ModelState))
            {
                response.OperationSuccess = false;
                response.Merge(student.ValidationErrors);

                return new ObjectResult(response);
            }

            var insertResponse = this.studentsBAL.Insert(student);
            response.Merge(insertResponse);

            if (!insertResponse.OperationSuccess)
            {
                return new ObjectResult(response);
            }

            return CreatedAtRoute("GetById", new { id = response.Item.Id }, response);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(long id, [FromBody] Student item)
        {
            var response = new Response<Student>();
            if (item == null || item.Id != id)
            {
                response.OperationSuccess = false;
                return BadRequest(response);
            }

            if (!item.IsValid(this.ModelState))
            {
                response.OperationSuccess = false;
                response.Merge(item.ValidationErrors);

                return BadRequest(response);
            }

            var studentResponse = await this.studentRepo.GetByIdAsync(item.Id);
            if (!studentResponse.OperationSuccess || studentResponse.Item == null)
            {
                response.OperationSuccess = false;
                return NotFound(response);
            }

            var student = studentResponse.Item;

            student.FirstMidName = item.FirstMidName;
            student.LastName = item.LastName;
            student.EnrollmentDate = item.EnrollmentDate;

            this.studentRepo.Update(student);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            this.studentRepo.Delete(id);
            return new NoContentResult();
        }
    }
}