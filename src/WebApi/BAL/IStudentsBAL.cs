using System.Threading.Tasks;
using ContosoUniversity.Models;

namespace ContosoUniversity.BAL
{
    public interface IStudentsBAL
    {
        Task<InquiryResponse<Student>> FetchAllAsync(InquiryRequest request);
       
        Response<Student> Insert(Student entity);
    }
}