using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContosoUniversity.DAL;
using ContosoUniversity.Models;

namespace ContosoUniversity.BAL
{
    public class StudentsBAL : IStudentsBAL

    {
        private readonly IStudentsRepository studentsRepo;

        private readonly IUnitOfWork unitOfWork;

        private List<string> validFilters = new List<string>();

        private InquiryResponse<Student> ValidateFilters(List<Filter> filters)
        {
            InquiryResponse<Student> response = new InquiryResponse<Student>();
            if (filters == null) return response;

            foreach (var filter in filters)
            {
                if (validFilters.FirstOrDefault(prop => prop == filter.Name) == null)
                {
                    response.OperationSuccess = false;
                    response.Messages.Add(new Message
                    {
                        Type = MessageType.VALIDATION,
                        Code = "E0002",
                        Arguments = new List<string> {
                             filter.Name,
                             filter.Value
                         }
                    });
                }
            }

            return response;
        }

        public StudentsBAL(IStudentsRepository studentsRepo, IUnitOfWork uow)
        {
            this.unitOfWork = uow;
            this.studentsRepo = studentsRepo;
            // TODO
            // Add custom filter properties whenever necessary
            validFilters = typeof(Student).GetProperties().Select(p => p.Name).ToList();
            validFilters.Add("IncludeProperties");
        }

        public async Task<InquiryResponse<Student>> FetchAllAsync(InquiryRequest request)
        {
            var response = new InquiryResponse<Student>();
            response.Merge(this.ValidateFilters(request.Filters));

            if (response.OperationSuccess == false)
            {
                return response;
            }

            var results = await this.studentsRepo.FetchAllAsync(request);
            return results;
        }

        public Response<Student> Insert(Student entity)
        {
            this.studentsRepo.Insert(entity);

            return new Response<Student>
            {
                OperationSuccess = true,
                Item = entity
            };
        }
    }
}